// name alert
// let name = "Araks";
// let admin = name;
// alert(admin);


//seconds in days
 let days = 10;
const sec = 86400;

 for(let i = 1; i <= days; i++){
  let number = prompt("Please enter a number from 1 tot 10");
  // console.log(number);
  alert(`You have entered number  ${number}`);
 let secperday = prompt( "In " + number + " days" ,number * sec + " seconds");
    console.log(secperday);
 }

// Как объявить переменную в Javascript?
// var myVar ="some info"; // устаревший метод
// const myConst = 123;// значение не должно меняться
// let myLetVar = 123; //значение может менятся

//В чем разница между функцией prompt и confirm?
//prompt дает поле ввода для данных и возвращает стринг при нажатии на ок.
// Если ничего не ввели вернет пустую строку, а если нажать отмена вернет Null.
// confirm задает закрытый вопрос, ответ ДА или Нет. Возвращает true /false

//Что такое неявное преобразование типов? Приведите один пример.
// Так как JavaScript — это язык со слабой типизацией, значения могут быть конвертированы между различными типами автоматически. Это называют неявным приведением типов. Обычно такое происходит, когда в выражениях используют значения различных типов, вроде 1 == null, 2/’5'. Неявное преобразование типов может быть вызвано и контекстом выражения, вроде if (value) {…}, где value неявно приводится к логическому типу данных.
//  Существует оператор, который не вызывает неявного преобразование типов — это оператор строгого равенства, ===. Оператор нестрогого равенства, ==, с другой стороны, выполняет и операцию сравнения, и, если нужно, выполняет неявное преобразование типов.
//Существует 3 наиболее широко используемых преобразований: строковое, численное и логическое.

// String(123) // явное преобразование
// 123 + ''    // неявное преобразование

// Boolean(2)          // явное преобразование
// if (2) { ... }      // неявное преобразование в логическом контексте

// Number('123')   // явное преобразование
// +'123'          // неявное преобразование
// 123 != '456'    // неявное преобразование
// 4 > '5'         // неявное преобразование
// 5/null          // неявное преобразование